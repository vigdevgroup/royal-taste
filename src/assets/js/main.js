import '@rateyo/jquery/lib/es/jquery.rateyo'
import {Fancybox} from "@fancyapps/ui";
import 'slick-carousel/slick/slick'
import './jquery.formstyler'
import 'air-datepicker/dist/js/datepicker'

$(function () {

    $('.jq-styler').styler()

    $('.datepicker-field__input').datepicker()

    const rateYoOptions = {
        starWidth: '21px',
        ratedFill: '#FEAC9D',
        spacing: '4px',
        normalFill: 'none',
        fullStar: true,
        starSvg: '<svg width="21" height="19" viewBox="0 0 21 19" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
            '    <path d="M10.5 1.2281L12.944 6.71107L13.0614 6.97447L13.3482 7.00474L19.3181 7.63482L14.8587 11.6536L14.6445 11.8466L14.7043 12.1287L15.9499 18.0011L10.7498 15.0019L10.5 14.8578L10.2502 15.0019L5.05011 18.0011L6.29566 12.1287L6.35549 11.8466L6.14126 11.6536L1.6819 7.63482L7.65176 7.00474L7.93855 6.97447L8.05596 6.71107L10.5 1.2281Z"\n' +
            '          stroke="#FEAC9D"/>\n' +
            '</svg>'
    }

    $('.product-card__rating').rateYo({
        ...rateYoOptions,
        readOnly: true
    })

    $('.product-page__rating-rate').rateYo({
        ...rateYoOptions,
        readOnly: true
    })

    let currentFaq = null
    $('.faq-item').on('click', '.faq-item__head', function () {
        const item = this.closest('.faq-item')

        if (!currentFaq) {
            currentFaq = item
            $(item).addClass('active')
            $(item).find('.faq-item__body').slideDown(400)
        } else if (currentFaq == item) {
            currentFaq = null
            $(item).removeClass('active')
            $(item).find('.faq-item__body').slideUp(400)
        } else {
            $(currentFaq).removeClass('active')
            $(currentFaq).find('.faq-item__body').slideUp(400)

            currentFaq = item

            $(item).addClass('active')
            $(item).find('.faq-item__body').slideDown(400)
        }
    })

    $('.reviews__slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        prevArrow: '<button class="reviews__arrow reviews__arrow_prev">\n' +
            '                            <svg width="11" height="19" viewBox="0 0 11 19" fill="none"\n' +
            '                                 xmlns="http://www.w3.org/2000/svg">\n' +
            '                                <path d="M9.72949 17.9584L1.27116 9.50008L9.72949 1.04175" stroke="#FEAC9D"\n' +
            '                                      stroke-width="1.5"\n' +
            '                                      stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '                            </svg>\n' +
            '                        </button>',
        nextArrow: '<button class="reviews__arrow reviews__arrow_next">\n' +
            '                            <svg width="11" height="19" viewBox="0 0 11 19" fill="none"\n' +
            '                                 xmlns="http://www.w3.org/2000/svg">\n' +
            '                                <path d="M1.27051 17.9584L9.72884 9.50008L1.27051 1.04175" stroke="#FEAC9D"\n' +
            '                                      stroke-width="1.5"\n' +
            '                                      stroke-linecap="round" stroke-linejoin="round"/>\n' +
            '                            </svg>\n' +
            '                        </button>'
    })

    $('.product-page__photo').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        draggable: false,
        asNavFor: '.product-page__gallery'
    })

    $('.product-page__gallery').slick({
        slidesToScroll: 1,
        slidesToShow: 3,
        asNavFor: '.product-page__photo',
        focusOnSelect: true,
        prevArrow: '<button class="product-page__gallery-arrow product-page__gallery-arrow_prev"></button>',
        nextArrow: '<button class="product-page__gallery-arrow product-page__gallery-arrow_next"></button>'
    })

    $('.info-btn').on('click', function () {
        openModal('info-module')
    })

    $('.review-btn').on('click', function () {
        openModal('review-module')
        $('.review-input__field').rateYo({
            ...rateYoOptions,
            starWidth: '28px',
            spacing: '5px'
        })
    })

    function openModal(module) {
        const modal = document.querySelector('.modal')
        const moduleHTML = document.querySelector('.' + module).innerHTML

        modal.querySelector('.modal__body').innerHTML = moduleHTML

        // $(modal).find('input[type="tel"]').each(function () {
        //     $(this).mask('+7 (000) 000 00 00', {
        //         onChange: function (cep, e) {
        //             checkPhone(cep, e);
        //         }
        //     });
        // });

        document.body.style.overflow = 'hidden'
        modal.style.display = 'flex'
        setTimeout(() => {
            modal.classList.add('show')
        }, 0)

        document.addEventListener('click', closeModal)
        document.addEventListener('keyup', closeModal)
    }

    function closeModal(event) {
        if ((event.target && (event.target.classList.contains('modal') || event.target.closest('.close')))
            || event.code === 'Escape') {
            const modal = document.querySelector('.modal')

            modal.classList.remove('show')
            document.body.style.overflow = ''

            setTimeout(() => {
                modal.style.display = ''
            }, 300)
        }
    }

    $('.quantity-input.jq-number').each(function() {
        const input = $(this).find('.quantity-input').get(0)

        if (input.min) {
            if (input.value <= input.min) $(this).find('.jq-number__spin.minus').addClass('disabled')
            else $(this).find('.jq-number__spin.minus').removeClass('disabled')
        }

        if (input.max) {
            if (input.value >= input.max) $(this).find('.jq-number__spin.plus').addClass('disabled')
            else $(this).find('.jq-number__spin.plus').removeClass('disabled')
        }
    })

    $('.quantity-input.jq-number').on('change', function () {
        const input = $(this).find('.quantity-input').get(0)

        if (input.min) {
            if (input.value <= input.min) $(this).find('.jq-number__spin.minus').addClass('disabled')
            else $(this).find('.jq-number__spin.minus').removeClass('disabled')
        }

        if (input.max) {
            if (input.value >= input.max) $(this).find('.jq-number__spin.plus').addClass('disabled')
            else $(this).find('.jq-number__spin.plus').removeClass('disabled')
        }
    })

})