import 'normalize.css'
import 'slick-carousel/slick/slick.css'
import '@rateyo/jquery/lib/es/jquery.rateyo.css'
import '@fancyapps/ui/dist/fancybox.css'
import '../assets/styles/libs/jquery.formstyler.css'
import '../assets/styles/libs/jquery.formstyler.theme.css'
import 'air-datepicker/dist/css/datepicker.css'
import '@styles/style.scss';
