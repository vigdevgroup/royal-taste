// Main
import '../assets/img/logo.png'
import '../assets/img/logo-opacity.png'


// Static
import '../assets/img/favorite.svg'
import '../assets/img/cart.svg'
import '../assets/img/search.svg'
import '../assets/img/profile.svg'
import '../assets/img/instagram.svg'
import '../assets/img/telegram.svg'
import '../assets/img/whatsapp.svg'
import '../assets/img/visa.png'
import '../assets/img/mastercard.png'
import '../assets/img/google-pay.png'
import '../assets/img/apple-pay.png'
import '../assets/img/mir.png'
import '../assets/img/main-bg.png'
import '../assets/img/gift.svg'
import '../assets/img/delivery.svg'
import '../assets/img/sale.svg'
import '../assets/img/diamond.svg'
import '../assets/img/plant.svg'
import '../assets/img/cook.svg'
import '../assets/img/evaluate-img.png'
import '../assets/img/info.svg'
import '../assets/img/big-phone.svg'
import '../assets/img/order-banner.jpg'
import '../assets/img/long-arrow-left.svg'
import '../assets/img/long-arrow-right.svg'
import '../assets/img/404.png'


// Content
import '../assets/img/content/partner.png'
import '../assets/img/content/partner-2.png'
import '../assets/img/content/partner-3.png'
import '../assets/img/content/popular.png'
import '../assets/img/content/product-1.png'
import '../assets/img/content/inst-1.png'
import '../assets/img/content/inst-2.png'
import '../assets/img/content/inst-3.png'
import '../assets/img/content/inst-5.png'
import '../assets/img/content/inst-6.png'
import '../assets/img/content/inst-7.png'
import '../assets/img/content/inst-8.png'
import '../assets/img/content/certificate.png'
import '../assets/img/content/partner-section-1.png'
import '../assets/img/content/partner-section-2.png'
import '../assets/img/content/partner-section-3.png'