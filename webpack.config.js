const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const autoprefixer = require('autoprefixer');
const webpack = require('webpack')

const isDev = process.env.NODE_ENV === 'development';
const isProd = !isDev;

const cssLoaders = (extra) => {
    let loaders = [
        {
        loader: MiniCssExtractPlugin.loader,
        options: {
            hmr: isDev,
            reloadAll: isDev
        }
        }, 
        'css-loader',
        {
            loader: 'postcss-loader',
            options: {
                plugins: [
                    autoprefixer({
                        overrideBrowserslist:['ie >= 8', 'last 4 version']
                    })
                ],
                sourceMap: true
            }
        }
    ]

    if (extra){
        loaders.push(extra);
    }
    
    return loaders;
}

const optimization = () => {
    let opt = {
        splitChunks: {
            chunks: 'all'
        }
    }

    if (isProd){
        opt.minimizer = [
            new TerserPlugin(),
            new OptimizeCssAssetsPlugin()
        ]
    }

    return opt
}

const jsLoader = (preset) => {
    let loader = {
        loader: 'babel-loader',
        options: {
            presets: [
                '@babel/preset-env'
            ],
            plugins: ['@babel/plugin-proposal-class-properties']
        }
    }

    if (preset){
        loader.options.presets.push(preset);
    }

    return loader;
}

module.exports = {
    entry: {
        'main': ['@babel/polyfill', '@src/index.js'],
    },
    module:{
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: jsLoader()
            },
            {
                test: /\.css$/,
                use: cssLoaders()
            },
            {
                test: /\.s[ac]ss$/,
                use: cssLoaders('sass-loader')
            },
            {
                test: /\.(png|jpg|svg|gif)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]'
                    }
                }]
            },
            {
                test: /\.(ttf|woff|woff2|eot)$/,
                use: ['file-loader']
            },
            {
                test: /\.csv$/,
                use: ['file-loader']
            },
            {
                test: /\.xml$/,
                use: ['file-loader']
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/about.html',
            filename: "about.html"
        }),
        new HtmlWebpackPlugin({
            template: './src/catalog.html',
            filename: "catalog.html"
        }),
        new HtmlWebpackPlugin({
            template: './src/404.html',
            filename: "404.html"
        }),
        new HtmlWebpackPlugin({
            template: './src/reviews.html',
            filename: "reviews.html"
        }),
        new HtmlWebpackPlugin({
            template: './src/product.html',
            filename: "product.html"
        }),
        new HtmlWebpackPlugin({
            template: './src/checkout.html',
            filename: "checkout.html"
        }),
        new HtmlWebpackPlugin({
            template: './src/cart.html',
            filename: "cart.html"
        }),
        new HtmlWebpackPlugin({
            template: './src/wishlist.html',
            filename: "wishlist.html"
        }),
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: isProd ? '[name].[hash].css' : '[name].css'
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ],
    devServer: {
        port: 4200,
        hot: isDev
    },
    devtool: isDev ? 'source-map' : '',
    resolve: {
        alias: {
            '@src': path.resolve(__dirname, 'src'),
            '@styles': path.resolve(__dirname, 'src/assets/styles'),
            '@data': path.resolve(__dirname, 'src/data'),
            '@img': path.resolve(__dirname, 'src/assets/img'),
            '@js': path.resolve(__dirname, 'src/assets/js'),
        }
    },
    optimization: optimization(),
    output: {
        path: path.resolve(__dirname, 'docs'),
        filename: isProd ? '[name].[hash].js' : '[name].js'
    },
}